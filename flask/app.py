from flask_api import FlaskAPI
from flask import send_file, send_from_directory, abort, redirect
from Tools import crawler, translate, youtube, texttospeech, frames
from flask_cors import CORS
from flask import request
import shutil
import ssl
import glob, os, re
import time
import zipfile
import cv2, random, shutil
from werkzeug.utils import secure_filename
import atexit
from apscheduler.scheduler import Scheduler

app = FlaskAPI(__name__)
app.config['MEDIA_FOLDER'] = 'media'
app.config['IMAGES'] = 'media/images'
app.config['TEXT_TO_SPEECH'] = 'media/textToSpeech'
app.config['YOUTUBE'] = 'media/youtube'
app.config['FRAMES'] = 'media/frames'

app.secret_key = 'harish'
CORS(app)

cron = Scheduler(daemon=True)
cron.start()
print(app.root_path)
@app.route('/')
def home():
    return {"message":"Smart tools"}

@cron.interval_schedule(hours=2)
def removeAllOldFiles():
    try:
        files = glob.glob('./media/**/**/**')
        for file in files:
            diffTime = time.time()-os.path.getmtime(file)
            if (diffTime>(3600*48)):
                if os.path.isdir(file):
                    shutil.rmtree(file)
                elif os.path.isfile(file):
                    os.remove(file)
        folders = glob.glob('./media/**/**')
        for folder in folders:
            diffTime = time.time()-os.path.getmtime(folder)
            if len(os.listdir(folder))==0 and (diffTime>(3600*48)):
                os.rmdir(folder)
    except:
        pass
@app.route('/done')
def done():
    return redirect('/media/youtube/fRWwI6ASCQabmBvd6Cr0dPUYAft1/aus.mp4')

searchId=''
type=''
@app.route('/imageslist',methods=['GET', 'POST'])
def imagelist():
    global searchId
    global type
    data = request.json['query']
    userId=data['userId']
    search = data['search']
    datatype = data['type']
    searchId=search
    type=datatype
    images = os.listdir(app.config["MEDIA_FOLDER"]+'/'+type+'/'+userId+'/'+search)
    return images

@app.route('/viewimage/<string:image>&<string:userId>',methods=['GET', 'POST'])
def viewimage(image, userId):
    global searchId
    global type
    return send_file(app.config["MEDIA_FOLDER"]+'/'+type+'/'+userId+'/'+searchId+'/'+image)

@app.route('/download/<string:userId>&<string:fileName>&<string:type>',methods=['GET', 'POST'])
def downloadZip(userId,fileName,type):
    try:
        return send_from_directory(app.config["MEDIA_FOLDER"]+'/'+type+'/'+userId+'/', filename=fileName, as_attachment=True)
    except FileNotFoundError:
        abort(404)

@app.route('/play/<string:userId>&<string:file>&<string:type>',methods=['GET', 'POST'])
def play(userId, file, type):
    print(type)
    if type=="video":
        return send_file(app.config['YOUTUBE']+'/'+userId+'/'+file)
    else:
        return send_file(app.config['TEXT_TO_SPEECH']+'/'+userId+'/'+file)



intervals = (
    ('weeks', 604800),  # 60 * 60 * 24 * 7
    ('days', 86400),    # 60 * 60 * 24
    ('hours', 3600),    # 60 * 60
    ('minutes', 60),
    ('seconds', 1),
    )

def display_time(seconds, granularity=2):
    result = []

    for name, count in intervals:
        value = seconds // count
        if value:
            seconds -= value * count
            if value == 1:
                name = name.rstrip('s')
            result.append("{} {}".format(value, name))
    return ', '.join(result[:granularity])

@app.route('/deleteRecent', methods=['DELETE'])
def deleteRecents():
    data = request.json['zipName']
    user = request.json['userId']
    type = request.json['type']
    path = app.config['MEDIA_FOLDER']+'/'+type+'/'+user+'/'+data
    os.remove(path)
    if type=='images':
        shutil.rmtree(path.split('.')[0])
    elif type=='frames':
        shutil.rmtree(path.split('.')[0])
    return "Deleted successfully"

@app.route('/recents',methods=['GET', 'POST'])
def recents():
    data = request.json['query']
    # print(data)
    userId=data['userId']
    type=data['type']
    recents = []
    files = glob.glob("./media/"+type+"/"+userId+'/'+"*.*")
    files.sort(reverse=True,key=os.path.getmtime)

    no_of_files = len(files)
    if no_of_files<6:
        for i in range(no_of_files):
            diffTime = display_time(int(time.time()-os.path.getmtime(files[i])))
            if os.path.basename(files[i]).split('.')[1]=='zip':
                zip = zipfile.ZipFile(files[i])
                recents.append({'filename':os.path.basename(files[i]),'size':len(zip.namelist()), 'time':diffTime})
            else:
                recents.append({'filename':os.path.basename(files[i]), 'time':diffTime})
    else:
        for i in range(5):
            diffTime = display_time(int(time.time()-os.path.getmtime(files[i])))
            if os.path.basename(files[i]).split('.')[1]=='zip':
                zip = zipfile.ZipFile(files[i])
                recents.append({'filename':os.path.basename(files[i]),'size':len(zip.namelist()), 'time':diffTime})
            else:
                recents.append({'filename':os.path.basename(files[i]), 'time':diffTime})
    return recents

@app.route('/images',methods=['GET', 'POST'])
def image():
    data = request.json['query']
    if data['imageCount']==0:
        return {"message":"Image count must be greater than 0"}, 422

    zipName = crawler.imageDownloader(search=data['searchKeyword'], count=data['imageCount'],searchEngine=data['searchEngine'],userId=data['userId'])
    if zipName:
        return {"zipName":zipName}
    else:
        return {"message":"Download failed"}, 503

@app.route('/translate',methods = ['GET', 'POST'])
def textTranslate():
    data = request.json['text']
    dest = request.json['dest']
    translatedText = translate.translateText(data, dest)
    return translatedText

@app.route('/textospeech',methods = ['GET', 'POST'])
def textToSpeech():
    audioName = request.json['saveAs']
    audioName = re.sub('[^A-Za-z0-9]+', '_', audioName)
    text = request.json['text']
    userId = request.json['userId']
    ext = request.json['ext']
    audioFolder = app.config['TEXT_TO_SPEECH']+'/'+userId
    if os.path.exists(audioFolder):
        pass
    else:
        os.makedirs(audioFolder)
    audioPath = audioFolder+"/"+audioName
    result = texttospeech.textAudio(audioName, text, audioPath, userId, ext)
    return result


@app.route('/youtube',methods = ['GET', 'POST'])
def youTube():
    ssl._create_default_https_context = ssl._create_stdlib_context
    link = request.json['link']
    userId = request.json['userId']
    saveAs = request.json['saveAs']
    saveAs = re.sub('[^A-Za-z0-9]+', '_', saveAs)
    type = request.json['type']
    result = youtube.youtubeDownloader(link, userId, saveAs, type)
    return result

@app.route('/frames',methods=['GET', 'POST'])
def videoFrames():
    print(request.data['userId'])
    userId = request.data['userId']
    videoSavePath = app.config['FRAMES']+'/'+userId+'/'
    if os.path.exists(videoSavePath):
        pass
    else:
        os.makedirs(videoSavePath)
    if request.method == 'POST':
        f = request.files['file']
        print(f.filename)
        videoFileName = re.sub('[^A-Za-z0-9]+', '_', f.filename)
        f.save(os.path.join(videoSavePath,secure_filename(videoFileName)))
    else:
        return {"error":"file not found"}
    path=videoSavePath+videoFileName
    print(path)
    frameAt=int(request.data['frameat'])
    result = frames.Frames(path, frameAt,userId)
    return result

atexit.register(lambda: cron.shutdown(wait=False))
if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')