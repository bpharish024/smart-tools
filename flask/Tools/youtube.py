import glob, os
from pytube import YouTube
from flask import request
from app import *
import random

def youtubeDownloader(link, userId, saveAs, type):
    folderPath='media/youtube/'+userId+'/'
    try:
        if os.path.exists(folderPath):
            raise FileExistsError
        os.makedirs(folderPath)
    except FileExistsError:
        pass
        # randomFilename = random.randint(1,1000)
        # os.makedirs(folderPath+str(randomFilename))
        # folderPath = folderPath+str(randomFilename)
    try:
        yt = YouTube(link)
    except Exception as e:
        print(e.__class__.__name__)
        # return {'link':"invalid",'message':e.__class__.__name__+' Exception Raised(1)!!!!, Try other link'}

        if e.__class__.__name__=='RegexMatchError':
            return {'link':"invalid",'message':'Invalid Link, Please enter valid link'}

        elif e.__class__.__name__=='VideoUnavailable':
            return {'link':"invalid",'message':'Failed to fetch video, VideoUnavailable error raised'}

        else:
            return {'link':"invalid",'message':e.__class__.__name__+' Exception Raised(1)!!!!, Try other link'}

    try:
        print(type)
        print(yt.streams[0].mime_type.split('/')[1])
        if type=="Audio":
            YouTube(link).streams.filter(only_audio=True).first().download(output_path=folderPath,filename=saveAs)
        else:
            YouTube(link).streams.first().download(output_path=folderPath,filename=saveAs)

        removeOldVideos(userId)
        return {'title':saveAs+'.'+yt.streams[0].mime_type.split('/')[1], 'res':YouTube(link).streams.first().resolution}

    except Exception as e:
        return {'link':"invalid",'message':e.__class__.__name__+' Exception Raised(2)!!!!, Try other link'}

def removeOldVideos(userId):
    files = glob.glob("./media/youtube/"+userId+"/*.*")
    files.sort(key=os.path.getmtime)
    no_of_files = len(files)
    if no_of_files>5:
        for i in range(no_of_files-5):
            os.remove(files[i])