import os
import random
import glob
from icrawler.builtin import GoogleImageCrawler, BingImageCrawler, BaiduImageCrawler
import shutil
from app import *
from datetime import date

def imageDownloader(search, count, searchEngine, userId):
    print(search)
    searchEngine = searchEngine
    filters = dict(
        size='large',
        license='commercial,modify',)
    searchKeyword = search
    searchKeyword = re.sub('[^A-Za-z0-9]+', '_', searchKeyword)
    folderPath = app.config['IMAGES']+'/'+userId+'/'+searchKeyword
    try:
        if os.path.exists(folderPath+'.zip') or os.path.exists(folderPath):
            raise FileExistsError
        os.makedirs(folderPath)
    except FileExistsError:
        randomFilename = random.randint(1,1000)
        os.makedirs(folderPath+str(randomFilename))
        folderPath = folderPath+str(randomFilename)

    if searchEngine=="Bing":
        crawler = BingImageCrawler(
            feeder_threads=2,
            parser_threads=4,
            downloader_threads=8,
            storage={'root_dir': folderPath})
        print(searchEngine)
    elif searchEngine=="Google":
        crawler = GoogleImageCrawler(
            feeder_threads=2,
            parser_threads=4,
            downloader_threads=8,
            storage={'root_dir': folderPath})
        print(searchEngine)
    else:
        crawler = BaiduImageCrawler(
            feeder_threads=2,
            parser_threads=4,
            downloader_threads=8,
            storage={'root_dir': folderPath})
        print(searchEngine)

    try:
        crawler.crawl(keyword=searchKeyword, max_num=int(count))
    except Exception as e:
        print("******************",type(e).__name__)
    list = os.listdir(folderPath)
    number_files = len(list)
    if number_files==0:
        shutil.rmtree(folderPath)
        return False
    zipDownloaded(folderPath)
    print("***********",folderPath)
    zipFileName = os.path.basename(folderPath)+'.zip'
    removeOldZips(userId)
    return zipFileName

def zipDownloaded(path):
    shutil.make_archive(path, 'zip', path)
    files = os.listdir(path)
    if len(files)>12:
        print(len(files))
        os.mkdir(path+"_temp/")
        for file in files[:12]:
            shutil.move(path+'/'+file, path+"_temp/")
        shutil.rmtree(path)
        os.rename(path+"_temp",path)

def removeOldZips(userId):
    files = glob.glob("./media/images/"+userId+"/*.zip")
    files.sort(key=os.path.getmtime)
    no_of_files = len(files)
    if no_of_files>6:
        for i in range(no_of_files-6):
            os.remove(files[i])
    floders = [folder for folder in glob.glob("./media/images/"+userId+"/*") if os.path.isdir(folder)]
    floders.sort(key=os.path.getmtime)
    no_of_folder = len(floders)
    if no_of_folder>6:
        for i in range(no_of_folder-6):
            shutil.rmtree(floders[i])
