from googletrans import Translator

def translateText(text, dest):
    translator = Translator()
    try:
        translatedText = translator.translate(text, dest=dest)
    except ValueError:
        return {"message":"invalid destination language"}, 400
    print(type(translatedText))
    translatedTextResults = {
        'src':translatedText.src,
        'dest':translatedText.dest,
        'text':translatedText.text,
        'pronunciation':translatedText.pronunciation,
        'extra_data':translatedText.extra_data

    }
    return translatedTextResults