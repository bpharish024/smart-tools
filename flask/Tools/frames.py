import os, cv2, random, shutil, glob

def Frames(path, frameAt, userId):
    frameName= os.path.basename(path).split(".")[0]
    framefolderPath="./media/frames/"+userId+'/'+frameName
    try:
        if os.path.exists(framefolderPath+'.zip') or os.path.exists(framefolderPath):
            raise FileExistsError
        os.makedirs(framefolderPath)
    except FileExistsError:
        randomFilename = random.randint(1,1000)
        os.makedirs(framefolderPath+str(randomFilename))
        framefolderPath = framefolderPath+str(randomFilename)
    vidObj = cv2.VideoCapture(path)
    count = 0
    success = 1
    cam = cv2.VideoCapture(path)
    fps = cam.get(cv2.CAP_PROP_FPS)
    print(framefolderPath)
    try:
        while success:
            success, image = vidObj.read()
            if count%(int(fps)*frameAt)==0:
                cv2.imwrite(framefolderPath+"/"+frameName+"_%d.jpg" % count, image)
            count += 1
        zipFramesDownloaded(framefolderPath)
        removeOldFrameZips(userId)
        os.remove(path)
        return {"frameZip":os.path.basename(framefolderPath)+'.zip'}

    except Exception as e:
        print(type(e).__name__)
        print(path)
        os.remove('./'+path)
        shutil.rmtree(framefolderPath)
        return {"exception" : type(e).__name__}, 422

def zipFramesDownloaded(path):
    shutil.make_archive(path, 'zip', path)
    files = os.listdir(path)
    if len(files)>12:
        print(len(files))
        os.mkdir(path+"_temp/")
        for file in files[:12]:
            shutil.move(path+'/'+file, path+"_temp/")
        shutil.rmtree(path)
        os.rename(path+"_temp",path)

def removeOldFrameZips(userId):
    files = glob.glob("./media/frames/"+userId+"/"+"*.zip")
    files.sort(key=os.path.getmtime)
    no_of_files = len(files)
    if no_of_files>6:
        for i in range(no_of_files-6):
            os.remove(files[i])
    floders = [folder for folder in glob.glob("./media/frames/"+userId+"/*") if os.path.isdir(folder)]
    floders.sort(key=os.path.getmtime)
    no_of_folder = len(floders)
    if no_of_folder>6:
        for i in range(no_of_folder-6):
            shutil.rmtree(floders[i])