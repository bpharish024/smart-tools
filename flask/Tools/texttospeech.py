import os, random, glob
from gtts import gTTS

def textAudio(audioName, text, audioPath, userId, ext):
    try:
        if os.path.exists(audioPath+ext):
            raise FileExistsError
    except FileExistsError:
        randomFilename = random.randint(1,1000)
        audioName = audioName+str(randomFilename)
    myobj = gTTS(text=text, lang='en', slow=False)
    myobj.save('media/textToSpeech/'+userId+'/'+audioName+ext)
    removeOldMp3(userId)
    return {'audioName' :audioName+ext}

def removeOldMp3(userId):
    files = glob.glob("./media/textToSpeech/"+userId+"/"+"*.*")
    files.sort(key=os.path.getmtime)
    no_of_files = len(files)
    if no_of_files>6:
        for i in range(no_of_files-6):
            os.remove(files[i])