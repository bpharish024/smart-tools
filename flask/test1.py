from icrawler.examples import GreedyImageCrawler

greedy_crawler = GreedyImageCrawler('media/images')
greedy_crawler.crawl(domains='bbc.com', max_num=5,
                     parser_thr_num=1, downloader_thr_num=1,
                     min_size=None, max_size=None)