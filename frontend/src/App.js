import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import './App.css';
import Home from './components/home'
import Navbar from './components/Nav'
import CollectImages from './components/tools/CollectImages';
import Translate from './components/tools/Translate';
import Texttospeech from './components/tools/TextToSpeech'
import Youtube from './components/tools/Youtube'
import Frames from './components/tools/Frames'
import Login from "./Login";
import SignUp from "./SignUp";
import { AuthProvider } from "./Auth";
import PrivateRoute from "./PrivateRoute";
import fPwd from './fPwd';
import UserSettings from './UserSettings'
import VideoPlayer from './components/VideoPlayer'
import ImageViewer from './components/tools/ImageViewer'
function App() {
  return (
    <AuthProvider>
    <BrowserRouter>
      <div className="App">
        <Navbar />
        <div className='container'>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/login' component={Login} />
          <Route path='/signup' component={SignUp}/>
          <Route path='/fpwd' component={fPwd}/>
          <PrivateRoute path='/play' component={VideoPlayer}/>
          <PrivateRoute path='/viewimage' component={ImageViewer}/>
          <PrivateRoute path='/settings' component={UserSettings}/>
          <PrivateRoute path='/images' component={CollectImages}/>
          <PrivateRoute path='/translate' component={Translate}/>
          <PrivateRoute path='/textospeech' component={Texttospeech}/>
          <PrivateRoute path='/youtube' component={Youtube}/>
          <PrivateRoute path='/frames' component={Frames}/>
        </Switch>
        </div>
      </div>
    </BrowserRouter>
    </AuthProvider>
  );
}

export default App;
