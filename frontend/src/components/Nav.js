import React, { useState, useContext } from 'react'
import { Link } from 'react-router-dom'
import app from "../base";
import { AuthContext } from "../Auth";

function Navbar() {
    var db = app.firestore();
    const { currentUser, setCurrentUser, user, setUser } = useContext(AuthContext);
    if (currentUser) {
        var docRef = db.collection("users").doc(currentUser.uid);
    }
    if (docRef) {
        docRef.get().then(function (doc) {
            if (doc.exists) {
                setUser(doc.data()['name'])
            } else {
                setUser("user")
            }
        }).catch(function (error) {
            console.log("Error getting document:", error);
        });
    }


    return (
        <nav className='navbar navbar-dark bg-dark mb-5 sticky-top'>
            <span className='navbar-brand mb-0 h1 mr-auto'><Link to={'/'} className="btn btn-dark">smartTools</Link></span>
            {currentUser
                ?<span>
            <span className='navbar-brand mb-0 h1 ml-auto'><small>{user}</small></span>
                <span>
                    <li class="navbar-brand nav-item dropdown">
                        <a class="nav-link btn-dark dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Options</a>
                        <div class="dropdown-menu">
                        <span className='dropdown-item border-bottom'><Link to="/settings" className="btn text-dark">Settings</Link></span>
                        <span onClick={() => {
                        app.auth().signOut()
                        setCurrentUser(null)
                    }} className='dropdown-item'><Link to="" className="btn text-dark">Logout</Link></span>
                        </div>
                    </li>
                    </span>
                    </span>
                : <span><span className='navbar-brand mb-0 h1'><a href='/signup' className="btn btn-dark">Register</a></span>
                    <span className='navbar-brand mb-0 h1'><Link to={'/login'} className="btn btn-dark">Login</Link></span>
                </span>
            }
        </nav>
    )
}

export default Navbar