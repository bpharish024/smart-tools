import React, { Component } from 'react'
import {Link} from 'react-router-dom'


export class ToolCards extends Component {

    render() {
        return (
            <React.Fragment>
            <div className="row">
                <div className="col-md-6">
                    <div className="card mb-4 bg-light shadow-sm">
                        <div className="card-header">
                            Collect Images
                        </div>
                        <div className="card-body">
                            <Link to={'images'} className="btn btn-dark">
                                <i className="fas fa-chevron-right"></i> Go
                            </Link>
                        </div>
                    </div>
                </div><div className="col-md-6">
                    <div className="card mb-4 bg-light shadow-sm">
                        <div className="card-header">
                        Text Translate
                        </div>
                        <div className="card-body">
                            <Link to={'translate'} className="btn btn-dark">
                                <i className="fas fa-chevron-right"></i> Go
                    </Link>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-6">
                    <div className="card mb-4 bg-light shadow-sm">
                        <div class="card-header">
                            Text To Audio
                        </div>
                        <div className="card-body">
                            <Link to={'textospeech'} className="btn btn-dark">
                                <i className="fas fa-chevron-right"></i> Go
                            </Link>
                        </div>
                    </div>
                </div><div className="col-md-6">
                    <div className="card mb-4 bg-light shadow-sm">
                        <div class="card-header">
                            Youtube Downloader
                        </div>
                        <div className="card-body">
                            <Link to={'youtube'} className="btn btn-dark">
                                <i className="fas fa-chevron-right"></i> Go
                    </Link>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-6">
                    <div className="card mb-4 bg-light shadow-sm">
                        <div class="card-header">
                            Extract Frames
                        </div>
                        <div className="card-body">
                            <Link to={'frames'} className="btn btn-dark">
                                <i className="fas fa-chevron-right"></i> Go
                            </Link>
                        </div>
                    </div>
                </div><div className="col-md-6">
                    <div className="card mb-4 bg-light shadow-sm">
                        <div class="card-header">
                            Upcoming
                        </div>
                        <div className="card-body">
                            <Link className="btn btn-dark">
                                <i className="fas fa-chevron-right"></i>
                    </Link>
                        </div>
                    </div>
                </div>
            </div>
            </React.Fragment>
        )
    }
}

export default ToolCards
