import React, { Component } from 'react'
import axios from 'axios'
import { PortAddress } from '../../PortAddress'
import Spinner from 'react-bootstrap/Spinner'
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { ToastsContainer, ToastsStore } from 'react-toasts';
import {languages} from './languages'
import { Link } from 'react-router-dom'

export class Translate extends Component {
    state = {
        loading: false,
        searchKeyword: '',
        results: '',
        dest: 'Kannada',
        result: false,
        hidden: 'invisible',
        copied: false,
        languages: languages
    }
    clear=()=> {
        this.setState({ searchKeyword:""})
    }
    onSelectLang = (e) => {
        this.setState({ dest: e.target.value })
    }
    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }
    translateText = (e) => {
        e.preventDefault()
        console.log(this.state.lang)
        this.setState(
            { loading: true }
        )
        this.setState(
            { result: false }
        )
        axios.post(`${PortAddress.server}/translate`, { text: this.state.searchKeyword, dest: this.state.dest })
            .then((response) => {
                console.log(response)
                this.setState(
                    { loading: false }
                )
                this.setState({ copied: false })
                this.state.results = response.data
                this.setState(
                    { result: true }
                )
                ToastsStore.success("Text Translated")

            })
            .catch(err => {
                console.log(err)
                if (err.response.status === 400) {
                    this.setState({ result: false })
                    this.setState({ loading: false })
                    ToastsStore.error("error!!")

                }
            })
    }


    render() {
        const languages = this.state.languages
        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-md-12">
                        <h1>Translate Text</h1>
                        <div className="jumbotron jumbotron-fluid border border-dark rounded">
                            <form onSubmit={this.translateText}>
                                <div className="form-group row justify-content-center">
                                    <label for="searchKeyword" className="col-sm-2 col-form-label">Text *</label>
                                    <div className="input-group col-md-6">
                                        <textarea type="text"
                                            onChange={this.onChange}
                                            name="searchKeyword"
                                            className="form-control"
                                            id="searchKeyword"
                                            placeholder="Text to translate"
                                            value={this.state.searchKeyword} required />
                                        <div class="input-group-append">
                                            <span style={{backgroundColor: "white",borderLeft:0,lineHeight:"inherit"}} class="btn input-group-text fas fa-times" name="searchKeyword" onClick={this.clear} id="basic-addon2"></span>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group row justify-content-center">
                                    <label htmlFor="lang" className="col-sm-2 col-form-label">Translate to *</label>
                                    <div className="col-md-6">
                                        <select name="dest" value={this.state.dest} onChange={this.onSelectLang} className="form-control">
                                            {languages.map(language=>(
                                                <option value={language}>{language}</option>
                                            ))}
                                        </select>
                                    </div>
                                </div>

                                <div className="form-group row justify-content-center">
                                    <div className="col-md-6">
                                        {this.state.loading
                                            ? <div><button variant="dark" disabled>
                                                <Spinner
                                                    as="span"
                                                    animation="grow"
                                                    size="sm"
                                                    role="status"
                                                    aria-hidden="true"
                                                />
                                        Translating...
                                        </button>
                                            </div>
                                            : <span>
                                            <button type="submit" className="btn btn-dark">Translate Text</button>
                                            <Link to={'/'} className="btn btn-dark ml-4"><i className="fas fa-arrow-left" aria-hidden="true"></i> Go Back</Link></span>
                                        }
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="row justify-content-center">
                    {this.state.result
                        ? <div className="col-md-6 visible">
                            <ul class="list-group">
                                <li class="list-group-item">Text translated : {this.state.results['text']}</li>
                                <li class="list-group-item">Pronunciation :{this.state.results['pronunciation'] ? this.state.results['pronunciation'] : "No data"}</li>
                                {/* <li class="list-group-item">{this.state.results['dest']}</li> */}
                            </ul>
                            <ToastsContainer store={ToastsStore} />
                            <CopyToClipboard text={this.state.results['text']}
                                onCopy={() => this.setState({ copied: true })}>
                                <span className="btn bg-light fas fa-copy pull-left"> Copy</span>
                            </CopyToClipboard>
                            {this.state.copied ? <span className="bg-light" style={{ color: 'green' }}> Copied.</span> : <span className="invisible" style={{ color: 'red' }}> Copied.</span>}
                        </div>

                        : <div className="col-md-6 invisible">
                            <ul class="list-group">
                            </ul>
                        </div>
                    }
                </div>
            </React.Fragment>

        )
    }
}

export default Translate
