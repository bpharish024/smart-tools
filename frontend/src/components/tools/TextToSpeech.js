import React, { Component } from 'react'
import axios from 'axios'
import ClipLoader from "react-spinners/ClipLoader";
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import {PortAddress} from '../../PortAddress'
import Spinner from 'react-bootstrap/Spinner'
import {ToastsContainer, ToastsStore} from 'react-toasts';
import { AuthContext } from "../../Auth";
import { Link } from 'react-router-dom'
import VideoPlayer from "../VideoPlayer"
import ReactDOM from 'react-dom'
import {deleteRecents} from '../../Functions'

export class TextToSpeech extends Component {
    static contextType = AuthContext;
    state = {
        loading: false,
        text: '',
        audio:'',
        saveAs:'',
        downloadLink:false,
        hidden:'invisible',
        recents:[],
        userId:this.context.currentUser.uid,
        type:'.mp3',
    }
    loadRecent() {
        const query={
            userId:this.state.userId,
            type:"textToSpeech"
        }
        axios.post(`${PortAddress.server}/recents`, { query })
            .then(response => {
                console.log(response)
                this.setState(
                    { recents: response.data }
                )
            })
            .catch(err => console.log(err))
    }
    componentDidMount() {
        this.loadRecent()
    }

    clearText=()=> {
        this.setState({text:""})
    }
    clearSave=()=> {
        this.setState({saveAs:""})
    }
    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }
    translateText = (e) => {
        e.preventDefault()
        if((this.state.saveAs).match(/[a-zA-Z0-9]+$/)===null){
            ToastsStore.error("Please dont use special characters for filename to save")
            return false
        }
        this.setState({ loading: true })
        this.setState({ downloadLink: false })
        this.setState({ audLink: '' })
        this.loadRecent()
        axios.post(`${PortAddress.server}/textospeech`,{ text: this.state.text, saveAs:this.state.saveAs, userId:this.state.userId, ext:this.state.type})
            .then((response) => {
                console.log(response)
                this.setState(
                    { audio: response.data.audioName })
                this.setState(
                    { loading: false })
                this.setState(
                    { downloadLink: true })
                    ToastsStore.success("Success")
            })
            .catch(err => {
                console.log(err)
                ToastsStore.error("Failed....!!!")
                this.setState(
                    { loading: false })
            })
    }
    play = (video) =>{
        console.log("hello")
        this.forceUpdate();
        this.setState({ openVideo: true })
        ReactDOM.render(
            <VideoPlayer open={this.state.openVideo} id={this.state.userId} video={video} type="textToSpeech"/>,
              document.getElementById('idd')
          )
          this.setState({ openVideo: false })
    }
    deleteRecent = (filename, youtube, uid)=>{
        deleteRecents(filename, youtube, uid, ()=>{
            console.log("loading")
            this.loadRecent()
        })
        console.log("delete recents")
    }
    download = (audio) => {
        fetch(`${PortAddress.server}/play/`+this.state.userId +"&" + audio+"&"+"textToSpeech", {
            responseType: 'blob',
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        })
            .then(response => response.blob())
            .then(blob => {
                const url = window.URL.createObjectURL(new Blob([blob]))
                const a = document.createElement('a')
                a.href = url
                a.setAttribute('download', audio)
                document.body.appendChild(a)
                a.click()
                a.parentNode.removeChild(a)
            })
    }
    onSelectType = (e) => {
        this.setState({ type: e.target.value })
    }
    render() {
        const recents = this.state.recents
        const uid = this.state.userId
        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-md-12">
                        <h1>Text To Audio</h1>
                         <div className="jumbotron jumbotron-fluid border border-dark rounded">

                        <form onSubmit={this.translateText}>
                            <div className="form-group row justify-content-center">
                                <label for="searchKeyword" className="col-sm-2 col-form-label">Text *</label>
                                <div className="input-group col-md-6">
                                    <textarea type="text"
                                        onChange={this.onChange}
                                        name="text"
                                        className="form-control"
                                        id="text"
                                        placeholder="Enter text to convert"
                                        value={this.state.text} required />
                                        <div class="input-group-append">
                                            <span style={{backgroundColor: "white",borderLeft:0,lineHeight:"inherit"}} class="btn input-group-text fas fa-times" name="text" onClick={this.clearText} id="basic-addon2"></span>
                                        </div>
                                </div>
                            </div>
                            <div className="form-group row justify-content-center">
                                    <label htmlFor="lang" className="col-sm-2 col-form-label">Output Type *</label>
                                    <div className="col-md-6">
                                        <select name="type" value={this.state.type} onChange={this.onSelectType} className="form-control">
                                                <option data-toggle="tooltip" title="MPEG-3" value=".mp3">MP3</option>
                                                <option data-toggle="tooltip" title="MPEG-4" value=".mp4">MP4</option>
                                                <option data-toggle="tooltip" title="Waveform Audio File Format" value=".wav">WAV</option>
                                                <option data-toggle="tooltip" title="Windows Media Audio format" value=".wma">WMA</option>
                                                <option data-toggle="tooltip" title="Free Lossless Audio Codec" value=".flac">FLAC</option>
                                                <option data-toggle="tooltip" title="audio-only MPEG-4 file" value=".m4a">M4A</option>
                                                <option data-toggle="tooltip" title="Advanced Audio Coding format" value=".aac">AAC</option>
                                        </select>
                                    </div>
                                </div>
                            <div className="form-group row justify-content-center">
                                <label for="lang" className="col-sm-2 col-form-label">Save file as *</label>
                                <div className="input-group col-md-6">
                                    <input type="text"
                                        onChange={this.onChange}
                                        name="saveAs"
                                        className="form-control"
                                        id="saveAs"
                                        placeholder="name to save audio file"
                                        value={this.state.saveAs} required />
                                        <div class="input-group-append">
                                            <span style={{backgroundColor: "white",borderLeft:0,lineHeight:"inherit"}} class="btn input-group-text fas fa-times" name="saveAs" onClick={this.clearSave} id="basic-addon2"></span>
                                        </div>
                                </div>
                            </div>

                            <div className="form-group row justify-content-center">
                                <div className="col-md-6">
                                    {this.state.loading
                                        ? <div><button variant="dark" disabled>
                                        <Spinner
                                            as="span"
                                            animation="grow"
                                            size="sm"
                                            role="status"
                                            aria-hidden="true"
                                        />
                                        Converting...
                                        </button>
                                    </div>
                                        : <span><button type="submit" className="btn btn-dark">Convert Text</button>
                                        <Link to={'/'} className="btn btn-dark ml-4"><i className="fas fa-arrow-left" aria-hidden="true"></i> Go Back</Link>
                                        </span>
                                    }
                                </div>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        {this.state.downloadLink
                            ? <div>
                                <button type='button' onClick={() => this.download(this.state.audio)} className="btn visible"><i className="fas fa-download" aria-hidden="true"></i> {this.state.audio}</button>
                            </div>
                            : <div>
                                <button type='button' onClick={this.download} className="btn invisible"><i className="fas fa-download" aria-hidden="true"></i> {this.state.audio}</button>
                            </div>
                        }
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-10 justify-content-center">
                        <div className="col-md-6 justify-content-center">
                            {recents.length>0
                            ?<span>
                            <h1>Recents <i data-toggle="tooltip" title="recents" className="btn fas fa-info-circle" aria-hidden="true"></i></h1>
                                {recents.map(recent => (
                                    <div className="list-group">
                                            <div class="btn-group dropup">
                                                <a style={{width: "100%", marginBottom:"5px"}} type="button" className="btn border border-dark list-group-item text-truncate text-dark bg-light">
                                                {recent['filename']} <small className="form-text text-muted">{recent['time']} ago</small>
                                                </a>
                                                <button style={{marginBottom:"5px"}} type="button" class="btn border border-dark dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <div class="dropdown-menu">
                                                    {/* <a onClick={() => this.play(recent['filename'])} className="btn dropdown-item">Play</a> */}
                                                    <a onClick={() => this.download(recent['filename'])} className="btn dropdown-item border-bottom">Download</a>
                                                    <a onClick={() => this.deleteRecent(recent['filename'], 'textToSpeech', uid)} className=" btn dropdown-item">Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                ))}
                            </span>
                            :<span></span>
                            }
                            </div>
                            <div id="idd">

                            </div>
                    </div>
                </div>
                <ToastsContainer store={ToastsStore}/>
            </React.Fragment>

        )
    }
}

export default TextToSpeech
