import React, { Component } from 'react'
import axios from 'axios'
import ClipLoader from "react-spinners/ClipLoader";
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import { PortAddress } from '../../PortAddress'
import Spinner from 'react-bootstrap/Spinner'
import { ToastsContainer, ToastsStore } from 'react-toasts';
import swal from 'sweetalert';
import { AuthContext } from "../../Auth";
import VideoPlayer from "../VideoPlayer"
import ReactDOM from 'react-dom'
import { Link } from 'react-router-dom'
import {deleteRecents} from '../../Functions'
export class Youtube extends Component {
    static contextType = AuthContext;
    state = {
        loading: false,
        link: '',
        video: '',
        video_id: '',
        resolution: '',
        downloadLink: false,
        hidden: 'invisible',
        invalidlink: false,
        invalidlinkMessage: '',
        userId: this.context.currentUser.uid,
        fileLink: '',
        saveAs: '',
        isActive: true,
        type: 'video',
        recents: [],
        openVideo:true
    }
    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }
    clearLink = (e) => {
        this.setState({ link: "" })
    }
    clearSave = (e) => {
        this.setState({ saveAs: "" })
    }
    youTubeDownload = (e) => {
        e.preventDefault()
        this.loadRecents()
        if ((this.state.saveAs).match(/[a-zA-Z0-9]+$/) === null) {
            ToastsStore.error("Please dont use special characters for filename to save")
            return false
        }
        this.setState({ loading: true })
        this.setState({ downloadLink: false })
        this.setState({ invalidlink: false })
        axios.post(`${PortAddress.server}/youtube`, { link: this.state.link, userId: this.state.userId, saveAs: this.state.saveAs, type: this.state.type })
            .then((response) => {
                console.log(response)
                if (response.data.link === 'invalid') {
                    this.setState({ invalidlinkMessage: response.data.message })
                    this.setState({ fileLink: response.data.ylink })
                    this.setState({ loading: false })
                    this.setState({ invalidlink: true })
                    ToastsStore.error(this.state.invalidlinkMessage)
                }
                else {
                    this.setState({ link: '' })
                    this.setState({ saveAs: '' })
                    this.setState({ video: response.data.title })
                    this.setState({ video_id: response.data.video_id })
                    this.setState({ resolution: response.data.res })
                    this.setState({ loading: false })
                    this.setState({ downloadLink: true })
                    console.log(this.state.video)
                    ToastsStore.success("Video Fetching Done")
                }
            })
            .catch(err => {
                console.log(err)
            })
    }
    swalAlert = () => {
        if (this.state.isActive) {
            console.log("inside", this.state.isActive)
            swal({
                title: 'Download',
                text: "loading...",
                timerProgressBar: true,
                buttons: false,
                closeOnClickOutside: false,
            })
        }
        else {
            console.log("inside", this.state.isActive)
            swal.close()
        }
    }
    loadRecents() {
        const query = {
            userId: this.state.userId,
            type:'youtube'
        }
        axios.post(`${PortAddress.server}/recents`, { query })
            .then(response => {
                console.log(response)
                this.setState(
                    { recents: response.data }
                )
            })
            .catch(err => console.log(err))
    }
    componentDidMount() {
        this.loadRecents()
    }
    swalAlert = () => {
        if (this.state.isActive) {
            console.log("inside", this.state.isActive)
            swal({
                title: 'Download',
                text: "loading...",
                timerProgressBar: true,
                buttons: false,
                closeOnClickOutside: false,
            })
        }
        else {
            console.log("inside", this.state.isActive)
            swal.close()
        }
    }
    onSelectType = (e) => {
        this.setState({ type: e.target.value })
    }
    play = (video) =>{
        this.forceUpdate();
        this.setState({ openVideo: true })
        ReactDOM.render(
            <VideoPlayer open={this.state.openVideo} id={this.state.userId} video={video} type="video"/>,
              document.getElementById('idd')
          )
          this.setState({ openVideo: false })
    }
    deleteRecent = (filename, youtube, uid)=>{
        deleteRecents(filename, youtube, uid, ()=>{
            console.log("loading")
            this.loadRecents()
        })
        console.log("delete recents")
    }
    download = (video) => {
        this.setState({ isActive: true })
        console.log("inside download", this.state.isActive)
        this.swalAlert()
        console.log("inside download", this.state.isActive)
        fetch(`${PortAddress.server}/play/` + this.state.userId + "&" + video + '&' + 'video', {
            responseType: 'blob',
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        })
            .then(response => response.blob())
            .then(blob => {
                this.setState({ isActive: false })
                this.swalAlert()
                const url = window.URL.createObjectURL(new Blob([blob]))
                const a = document.createElement('a')
                a.href = url
                a.setAttribute('download', video)
                document.body.appendChild(a)
                a.click()
                a.parentNode.removeChild(a)
                this.setState({ isActive: true })
            })

    }


    render() {
        const recents = this.state.recents
        const uid = this.state.userId
        return (
            <React.Fragment>
                <div className="row">
                <div id="idd">

                </div>
                    <div className="col-md-12">
                        <h1>YouTube Downloader</h1>
                        <div className="jumbotron border border-dark jumbotron-fluid rounded">
                            <form onSubmit={this.youTubeDownload}>
                                <div className="form-group row justify-content-center">
                                    <label for="searchKeyword" className="col-sm-2 col-form-label">YouTube link *</label>
                                    <div className="input-group col-md-6">
                                        <input type="text"
                                            onChange={this.onChange}
                                            name="link"
                                            className="form-control"
                                            id="link"
                                            placeholder="Enter youtube link"
                                            value={this.state.link} required />
                                        <div class="input-group-append">
                                            <span style={{ backgroundColor: "white", borderLeft: 0, lineHeight: "inherit" }} class="btn input-group-text fas fa-times" name="link" onClick={this.clearLink} id="basic-addon2"></span>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group row justify-content-center">
                                    <label htmlFor="lang" className="col-sm-2 col-form-label">Output Type *</label>
                                    <div className="col-md-6">
                                        <select name="type" value={this.state.type} onChange={this.onSelectType} className="form-control">
                                            <option value="Video">Video</option>
                                            <option value="Audio">Audio only</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group row justify-content-center">
                                    <label for="lang" className="col-sm-2 col-form-label">Save file as *</label>
                                    <div className="input-group col-md-6">
                                        <input type="text"
                                            onChange={this.onChange}
                                            name="saveAs"
                                            className="form-control"
                                            id="saveAs"
                                            placeholder="name to save video file"
                                            value={this.state.saveAs} required />
                                        <div class="input-group-append">
                                            <span style={{ backgroundColor: "white", borderLeft: 0, lineHeight: "inherit" }} class="btn input-group-text fas fa-times" name="saveAs" onClick={this.clearSave} id="basic-addon2"></span>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group row justify-content-center">
                                    <div className="col-md-6">
                                        {this.state.loading
                                            ? <div><button variant="dark" disabled>
                                                <Spinner
                                                    as="span"
                                                    animation="grow"
                                                    size="sm"
                                                    role="status"
                                                    aria-hidden="true"
                                                />
                                        Fetching Video...
                                        </button>
                                            </div>
                                            :<span> <button type="submit" className="btn btn-dark">Get video</button>
                                             <Link to={'/'} className="btn btn-dark ml-4"><i className="fas fa-arrow-left" aria-hidden="true"></i> Go Back</Link></span>
                                        }
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6 offset-5">
                        {this.state.downloadLink
                            ? <div>
                                <div className="list-group">
                                            <div class="btn-group dropup">
                                                <button type="button" class="btn border border-dark dropdown-toggle list-group-item text-truncate text-dark bg-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    {this.state.video}({this.state.resolution})
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a onClick={() => this.play(this.state.video)} className="btn dropdown-item border-bottom">Play</a>
                                                    <a onClick={() => this.download(this.state.video)} className="btn dropdown-item">Download</a>

                                                </div>
                                            </div>
                                        </div>
                            </div>
                            : <div>
                                <button type='button' onClick={() => this.download(this.state.video)} className="btn invisible"><i className="fas fa-download" aria-hidden="true"></i> {this.state.video}</button>
                            </div>
                        }
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        {this.state.invalidlink
                            ? <div>
                                <ToastsContainer store={ToastsStore} />
                            </div>
                            : <div className="btn invisible">
                            </div>
                        }
                    </div>

                </div>
                <div className="row">
                    <div className="col-md-10 justify-content-center">
                        {/* <RecentImages /> */}
                        <div className="col-md-6 justify-content-center">
                            {recents.length > 0
                                ? <span>
                                    <h1>Recents <i data-toggle="tooltip" title="recents" className="btn fas fa-info-circle" aria-hidden="true"></i></h1>
                                    {recents.map(recent => (
                                        <div className="list-group">
                                            <div class="btn-group dropup">
                                                <a style={{width: "100%", marginBottom:"5px"}} type="button" className="btn border border-dark list-group-item text-truncate text-dark bg-light">
                                                {recent['filename']} <small className="form-text text-muted">{recent['time']} ago</small>
                                                </a>
                                                <button style={{marginBottom:"5px"}} type="button" class="btn border border-dark dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a onClick={() => this.play(recent['filename'])} className="btn dropdown-item border-bottom">Play</a>
                                                    <a onClick={() => this.download(recent['filename'])} className="btn dropdown-item border-bottom">Download</a>
                                                    <a onClick={() => this.deleteRecent(recent['filename'], 'youtube', uid)} className=" btn dropdown-item">Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </span>
                                : <span></span>
                            }
                        </div>
                    </div>
                </div>
            </React.Fragment>

        )
    }
}

export default Youtube
