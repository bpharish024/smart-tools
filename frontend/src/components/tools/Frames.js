import React, { Component } from 'react'
import axios from 'axios'
import 'react-dropdown/style.css';
import { PortAddress } from '../../PortAddress'
import Spinner from 'react-bootstrap/Spinner'
import {ToastsContainer, ToastsStore} from 'react-toasts';
import { AuthContext } from "../../Auth";
import swal from 'sweetalert';
import { Link } from 'react-router-dom'
import {deleteRecents} from '../../Functions'

export class TextToSpeech extends Component {
    static contextType = AuthContext;
    state = {
        loading: false,
        selectedFile: null,
        frameat: '',
        frameZip: '',
        downloadLink: false,
        invalid: false,
        invalidMessage: '',
        recents: [],
        userId:this.context.currentUser.uid,
        isActive:true
    }
    loadFrames() {
        const query={
            userId:this.state.userId,
            type:'frames'
        }
        axios.post(`${PortAddress.server}/recents`, { query })
            .then(response => {
                console.log(response)
                this.setState(
                    { recents: response.data }
                )
            })
            .catch(err => console.log(err))
    }
    componentDidMount() {
        this.loadFrames()
    }
    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }
    onFileChange = (e) => {
        this.setState({ selectedFile: e.target.files[0] })
    }
    extractFrame = () => {
        const formData = new FormData();
        if ((this.state.selectedFile) && (this.state.frameat>0)) {
            formData.append('file', this.state.selectedFile, this.state.selectedFile.name);
            formData.append('frameat', this.state.frameat);
            formData.append('userId',this.state.userId);
        }
        else {
            return false
        }
        this.setState({ loading: true })
        this.setState({ downloadLink: false })
        this.setState({ invalid: false })
        this.loadFrames()
        axios.post(`${PortAddress.server}/frames`, formData)
            .then((response) => {
                console.log(response)
                this.setState(
                    { loading: false }
                )
                this.setState({ frameZip: response.data.frameZip })
                this.setState(
                    { downloadLink: true })
                    ToastsStore.success("Frames Extracted")
            })
            .catch(err => {
                console.log(err)
                if (err.response.status === 422) {
                    this.setState({ downloadLink: false })
                    this.setState({ loading: false })
                    this.setState({ invalid: true })
                    ToastsStore.error("Failed....!!!")
                }
            })
    }
    swalAlert=()=>{
        if(this.state.isActive){
            console.log("inside",this.state.isActive)
        swal({
            title: 'Download',
            text: "loading...",
            timerProgressBar: true,
            buttons: false,
            closeOnClickOutside: false,
        })
    }
    else{
        console.log("inside",this.state.isActive)
        swal.close()
    }
    }
    viewSample = (filename, frames) =>{
        console.log(filename.split("."))
        window.location.assign("/viewimage?search="+filename.split(".")[0]+"&type="+frames);

    }
    deleteRecent = (filename, frames, uid)=>{
        deleteRecents(filename, frames, uid, ()=>{
            console.log("loading")
            this.loadFrames()
        })
        console.log("delete recents")
    }
    download = (zipName) => {
        this.setState({isActive:true})
        console.log("inside download",this.state.isActive)
        this.swalAlert()
        fetch(`${PortAddress.server}/download/`+this.state.userId +"&" + zipName+ "&" + "frames", {
            responseType: 'blob',
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        })
        .then(response => response.blob())
            .then(blob => {
                this.setState({isActive:false})
                this.swalAlert()
                const url = window.URL.createObjectURL(new Blob([blob]))
                const a = document.createElement('a')
                a.href = url
                a.setAttribute('download', zipName)
                document.body.appendChild(a)
                a.click()
                a.parentNode.removeChild(a)
                this.setState({isActive:true})
            })

    }


    render() {
        const recents = this.state.recents
        const uid = this.state.userId
        return (
            <React.Fragment>
                <h1>Video Frames</h1>
                <div className="jumbotron jumbotron-fluid border border-dark rounded">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="form-group row justify-content-center">
                                <label for="searchKeyword" className="col-sm-2 col-form-label">Upload Video *</label>
                                <div className="col-md-6">
                                    <input type="file"
                                        onChange={this.onFileChange}
                                        name="file"
                                        className="form-control-file"
                                        id="file"
                                        required />
                                </div>
                            </div>
                            <div className="form-group row justify-content-center">
                                <label for="lang" className="col-sm-2 col-form-label">Frame at every * secs</label>
                                <div className="col-md-6">
                                    <input type="number"
                                        onChange={this.onChange}
                                        name="frameat"
                                        min='0'
                                        className="form-control"
                                        id="frameat"
                                        placeholder="Extract frame at every seconds"
                                        value={this.state.frameat} required />
                                </div>
                            </div>

                            <div className="form-group row justify-content-center">
                                <div className="col-md-6">
                                    {this.state.loading
                                        ? <div><button variant="dark" disabled>
                                            <Spinner
                                                as="span"
                                                animation="grow"
                                                size="sm"
                                                role="status"
                                                aria-hidden="true"
                                            />
                                        Extracting Frames...
                                        </button>
                                        </div>
                                        : <span><button type="submit" onClick={this.extractFrame} className="btn btn-dark">Extract Frames</button>
                                        <Link to={'/'} className="btn btn-dark ml-4"><i className="fas fa-arrow-left" aria-hidden="true"></i> Go Back</Link>
                                        </span>

                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12 offset-4">
                        {this.state.downloadLink
                            ? 
                            <div>
                                    <div className="list-group">
                                            <div class="btn-group dropup">
                                                <button type="button" class="btn border border-dark dropdown-toggle list-group-item text-truncate text-dark bg-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {this.state.frameZip}
                                            </button>
                                                <div class="dropdown-menu">
                                                    <a onClick={() => this.viewSample(this.state.frameZip,'frames')} className="btn dropdown-item border-bottom">view sample</a>
                                                    <a onClick={() => this.download(this.state.frameZip)} className=" btn dropdown-item">Download</a>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            : <div>
                                <button type='button' onClick={this.download} className="btn invisible"><i className="fas fa-download" aria-hidden="true"></i> {this.state.audio}</button>
                            </div>
                        }
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        {this.state.invalid
                            ? <div>
                                <p>Invalid data</p>
                            </div>
                            : <div className="invisible">
                            </div>
                        }
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-10 justify-content-center">
                        <div className="col-md-6 justify-content-center">
                            {recents.length>0
                            ?<span>
                            <h1>Recents <i data-toggle="tooltip" title="recents" className="btn fas fa-info-circle" aria-hidden="true"></i></h1>
                                {recents.map(recent => (
                                    <div className="list-group">
                                            <div class="btn-group dropup">
                                                <a style={{width: "100%", marginBottom:"5px"}} type="button" className="btn border border-dark list-group-item text-truncate text-dark bg-light">
                                                {recent['filename']} <small className="badge form-text text-muted">({recent['size']} images)</small><small className="form-text text-muted">{recent['time']} ago</small>
                                                </a>
                                                <button style={{marginBottom:"5px"}} type="button" class="btn border border-dark dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a onClick={() => this.viewSample(recent['filename'], 'frames')} className="btn dropdown-item border-bottom">view sample</a>
                                                    <a onClick={() => this.download(recent['filename'])} className="btn dropdown-item border-bottom">Download</a>
                                                    <a onClick={() => this.deleteRecent(recent['filename'], 'frames', uid)} className=" btn dropdown-item">Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                ))}
                            </span>
                            :<span></span>
                            }
                            </div>
                    </div>
                </div>
                <ToastsContainer store={ToastsStore}/>
            </React.Fragment>

        )
    }
}

export default TextToSpeech
