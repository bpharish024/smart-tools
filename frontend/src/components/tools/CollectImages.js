import React, { Component } from 'react'
import axios from 'axios'
import Spinner from 'react-bootstrap/Spinner'
import { PortAddress } from '../../PortAddress'
import { ToastsContainer, ToastsStore } from 'react-toasts';
import { AuthContext } from "../../Auth";
import swal from 'sweetalert';
import { Link } from 'react-router-dom'
import ImageViewer from './ImageViewer'
import { withRouter, Redirect } from "react-router";
import { deleteRecents } from '../../Functions'

export class CollectImages extends Component {
    static contextType = AuthContext;
    state = {
        searchKeyword: '',
        imageCount: '',
        loading: false,
        downloadLink: false,
        zipName: '',
        recents: [],
        searchEngine: 'Bing',
        nodata: false,
        userId: this.context.currentUser.uid,
        isActive: true
    }
    clear = () => {
        this.setState({ searchKeyword: "" })
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSelect = (e) => {
        this.setState({ searchEngine: e.target.value })
        console.log(this.state.searchEngine)
    }
    loadRecents() {
        const query = {
            userId: this.state.userId,
            type: 'images'
        }
        axios.post(`${PortAddress.server}/recents`, { query })
            .then(response => {
                console.log(response)
                this.setState(
                    { recents: response.data }
                )
            })
            .catch(err => console.log(err))
    }
    componentDidMount() {
        this.loadRecents()
    }
    swalAlert = () => {
        if (this.state.isActive) {
            console.log("inside", this.state.isActive)
            swal({
                title: 'Download',
                text: "loading...",
                timerProgressBar: true,
                buttons: false,
                closeOnClickOutside: false,
            })
        }
        else {
            console.log("inside", this.state.isActive)
            swal.close()
        }
    }

    download = (zipName) => {
        this.setState({ isActive: true })
        console.log("inside download", this.state.isActive)
        this.swalAlert()
        fetch(`${PortAddress.server}/download/` + this.state.userId + "&" + zipName + "&" + "images", {
            responseType: 'blob',
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        })
            .then(response => response.blob())
            .then(blob => {
                this.setState({ isActive: false })
                this.swalAlert()
                const url = window.URL.createObjectURL(new Blob([blob]))
                const a = document.createElement('a')
                a.href = url
                a.setAttribute('download', zipName)
                document.body.appendChild(a)
                a.click()
                a.parentNode.removeChild(a)
                this.setState({ isActive: true })
            })

    }
    viewSample = (filename, images) => {
        console.log(filename.split("."))
        window.location.assign("/viewimage?search=" + filename.split(".")[0] + "&type=" + images);

    }
    deleteRecent = (filename, image, uid) => {
        deleteRecents(filename, image, uid, () => {
            console.log("loading")
            this.loadRecents()
        })
        console.log("delete recents")
    }
    searchImages = (e) => {
        e.preventDefault();
        this.loadRecents()
        this.setState({ loading: true })
        this.setState({ downloadLink: false })
        this.setState({ nodata: false })
        const query = {
            searchKeyword: this.state.searchKeyword,
            imageCount: this.state.imageCount,
            searchEngine: this.state.searchEngine,
            userId: this.state.userId
        }
        console.log(this.state.searchEngine)
        axios.post(`${PortAddress.server}/images`, { query })
            .then(response => {
                console.log(response)
                this.setState({ searchKeyword: '' })
                this.setState({ imageCount: '' })
                this.setState({ loading: false })
                this.setState({ downloadLink: true })
                this.setState({ zipName: response.data['zipName'] })
                this.setState({ nodata: false })
                ToastsStore.success("Images Collected")
            })
            .catch(err => {
                if (err.response) {
                    console.log(err.response.status)
                    if (err.response.status === 503) {
                        this.setState({ loading: false })
                        this.setState({ nodata: true })
                        ToastsStore.error("Failed")
                    }
                    else if (err.response.status === 422) {
                        this.setState({ loading: false })
                        this.setState({ nodata: true })
                        ToastsStore.error("Failed")
                    }
                    else {
                        this.setState({ loading: false })
                        this.setState({ nodata: true })
                        ToastsStore.error("Failed")
                    }
                }
                else {
                    this.setState({ loading: false })
                    this.setState({ nodata: true })
                }
            })
    }
    render() {
        const recents = this.state.recents
        const uid = this.state.userId
        return (
            <React.Fragment>
                <div className="col-md-12">
                    <h1>Collect Images</h1>
                    <div className="jumbotron jumbotron-fluid border border-dark rounded">
                        <form onSubmit={this.searchImages}>
                            <div className="form-group row justify-content-center">
                                <label htmlFor="searchKeyword" className="col-sm-2 col-form-label">Search For *</label>
                                <div className="input-group col-md-6">
                                    <input type="text"
                                        onChange={this.onChange}
                                        name="searchKeyword"
                                        className="form-control"
                                        id="searchKeyword"
                                        placeholder="Search For"
                                        value={this.state.searchKeyword} required />
                                    <div class="input-group-append">
                                        <span style={{ backgroundColor: "white", borderLeft: 0, lineHeight: "inherit" }} class="btn input-group-text fas fa-times" name="searchKeyword" onClick={this.clear} id="basic-addon2"></span>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group row justify-content-center">
                                <label htmlFor="imageCount" className="col-sm-2 col-form-label">Required Images *</label>
                                <div className="col-md-6">
                                    <input type="number"
                                        onChange={this.onChange}
                                        name="imageCount"
                                        className="form-control"
                                        id="imageCount"
                                        pattern='[0-9]{0,3}'
                                        min="1" max="500"
                                        value={this.state.imageCount}
                                        placeholder="max 500 images" required />
                                </div>
                            </div>
                            <div className="form-group row justify-content-center">
                                <label htmlFor="searchEngine" className="col-sm-2 col-form-label">Search Engine *</label>
                                <div className="col-md-6">
                                    <select name="searchEngine" value={this.state.searchEngine} onChange={this.onSelect} className="form-control">
                                        <option value="Bing">Bing</option>
                                        <option disabled value="Google">Google</option>
                                        <option disabled value="Baidu">Baidu</option>
                                    </select>
                                </div>
                            </div>
                            <div className="form-group row justify-content-center">
                                <div className="col-md-6">
                                    {this.state.loading
                                        ? <div><button variant="dark" disabled>
                                            <Spinner
                                                as="span"
                                                animation="grow"
                                                size="sm"
                                                role="status"
                                                aria-hidden="true"
                                            />
                                        Collecting...
                                        </button>
                                        </div>
                                        : <span><button type="submit" className="btn btn-dark">Collect</button>
                                            <Link to={'/'} className="btn btn-dark ml-4"><i className="fas fa-arrow-left" aria-hidden="true"></i> Go Back</Link>
                                        </span>
                                    }
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="row">
                        <div className="col-md-6 offset-5">
                            {this.state.downloadLink
                                ? <div>
                                    <div className="list-group">
                                        <div class="btn-group dropup">
                                            {/* <a style={{ width: "30%", marginBottom: "5px" }} type="button" className="btn border border-dark list-group-item text-truncate text-dark bg-light">
                                                {this.state.zipName}
                                            </a>
                                            <button style={{ marginBottom: "5px" }} type="button" class="btn border border-dark dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button> */}
                                            <button type="button" class="btn border border-dark dropdown-toggle list-group-item text-truncate text-dark bg-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {this.state.zipName}
                                            </button>
                                            <div class="dropdown-menu">
                                                <a onClick={() => this.viewSample(this.state.zipName, 'images')} className="btn dropdown-item border-bottom">view sample</a>
                                                <a onClick={() => this.download(this.state.zipName)} className=" btn dropdown-item">Download</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                : <div>
                                    <button type='button' onClick={this.download} className="btn invisible"><i className="fas fa-download" aria-hidden="true"></i> {this.state.zipName}</button>
                                </div>
                            }
                        </div>
                        <div className="col-md-12">
                            {this.state.nodata
                                ? <div>
                                    <p className="bg-danger">Somthing went wrong, Try again</p>
                                </div>
                                : <div>
                                </div>
                            }
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-10 justify-content-center">
                            {/* <RecentImages /> */}
                            <div className="col-md-6 justify-content-center">
                                {recents.length > 0
                                    ? <span>
                                        <h1>Recents <i data-toggle="tooltip" title="recents" className="btn fas fa-info-circle" aria-hidden="true"></i></h1>
                                        {recents.map(recent => (
                                            <div className="list-group">
                                                <div class="btn-group dropup">
                                                    <a style={{ width: "100%", marginBottom: "5px" }} type="button" className="btn border border-dark list-group-item text-truncate text-dark bg-light">
                                                        {recent['filename']} <small>({recent['size']} images)</small> <small className="form-text text-muted">{recent['time']} ago</small>
                                                    </a>
                                                    <button style={{ marginBottom: "5px" }} type="button" class="btn border border-dark dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a onClick={() => this.viewSample(recent['filename'], 'images')} className="btn dropdown-item border-bottom">view sample</a>
                                                        <a onClick={() => this.download(recent['filename'])} className=" btn dropdown-item border-bottom">Download</a>
                                                        <a onClick={() => this.deleteRecent(recent['filename'], 'images', uid)} className=" btn dropdown-item">Delete</a>

                                                    </div>
                                                </div>
                                            </div>
                                        ))}
                                    </span>
                                    : <span></span>
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <ToastsContainer store={ToastsStore} />
            </React.Fragment>
        )
    }
}

export default CollectImages
