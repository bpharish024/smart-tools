import React from 'react'
import axios from 'axios'
import { PortAddress } from '../../PortAddress'
import { AuthContext } from "../../Auth";
import { Link } from 'react-router-dom'
import swal from 'sweetalert';

export class ImageViewer extends React.Component {
    static contextType = AuthContext;

    state = {
        images: [],
        userId: this.context.currentUser.uid,
        searchId:'',
        isActive:true,
        type:'',
    }
    close = () => {
        console.log("true")
        return false;
    }
    componentDidMount() {
        this.loadImagesUrl()
    }
    swalAlert=()=>{
        if(this.state.isActive){
            console.log("inside",this.state.isActive)
        swal({
            title: 'Download',
            text: "loading...",
            timerProgressBar: true,
            buttons: false,
            closeOnClickOutside: false,
        })
    }
    else{
        console.log("inside",this.state.isActive)
        swal.close()
    }
    }
    loadImagesUrl() {
        const urlparam = new URLSearchParams(this.props.location.search);
        const search = urlparam.get('search')
        const type = urlparam.get('type')
        this.setState({searchId:search})
        this.setState({type:type})
        console.log(search)//123 
        const query = {
            userId: this.state.userId,
            search,
            type
        }
        axios.post(`${PortAddress.server}/imageslist`, { query })
            .then(response => {
                this.setState({ images: response.data })
            })
            .catch(err => console.log(err))
    }
    download = () => {
        const urlparam = new URLSearchParams(this.props.location.search);
        const search = urlparam.get('search')
        const type = urlparam.get('type')
        this.setState({isActive:true})
        console.log("inside download",search)
        this.swalAlert()
        fetch(`${PortAddress.server}/downloadzip/` + this.state.userId + "&" + search+'.zip'+"&" + type, {
            responseType: 'blob',
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        })
            .then(response => response.blob())
            .then(blob => {
                this.setState({isActive:false})
                this.swalAlert()
                const url = window.URL.createObjectURL(new Blob([blob]))
                const a = document.createElement('a')
                a.href = url
                a.setAttribute('download', search+'.zip')
                document.body.appendChild(a)
                a.click()
                a.parentNode.removeChild(a)
                this.setState({isActive:true})
            })

    }

    render() {
        const images = this.state.images
        const urlparam = new URLSearchParams(this.props.location.search);
        const search = urlparam.get('search')
        const type = urlparam.get('type')
        console.log(images)
        return (

            <div>
                <div>
                <div className="row mb-4">
                <Link to={'/'+type} className="btn btn-info ml-3"><i className="fas fa-arrow-left" aria-hidden="true"></i> Go Back</Link>
                <button type='button' onClick={() => this.download()} className="btn visible ml-3"><i className="fas fa-download" aria-hidden="true"></i> Download</button>
                </div>
                    <div className="row row_img">
                        {images.map((image, index) => (
                            <div className="col-md-3 col_img" style={{height: "15rem"}}>
                                <img class="img-thumbnail img-fluid card-img-top h-100 zoom" style={{display:"block"}} key={new Date().getTime()} src={PortAddress.server + "/viewimage/" + image +'&'+ this.state.userId+'?dc=' + search} alt="..."/>
                            </div>
                        ))}

                    </div>
                </div>
            </div>
        )
    }
}
export default ImageViewer