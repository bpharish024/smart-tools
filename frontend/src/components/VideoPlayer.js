
import React from 'react'
import ReactDOM from 'react-dom'
import ModalVideo from 'react-modal-video'
import '../../node_modules/react-modal-video/scss/modal-video.scss';
import { withRouter, Redirect } from "react-router";
import { PortAddress } from '../PortAddress'
import ReactAudioPlayer from 'react-audio-player';

class VideoPlayer extends React.Component {
 
  constructor (props) {
    super(props)
    this.state = {
      isOpen: this.props.open,
      id:this.props.id,
      video:this.props.video,
      type:this.props.type
    }
    // this.openModal = this.openModal.bind(this)
  }
 
//   openModal () {
//     this.setState({isOpen: true})
//   }
  closeModel = () => {
    console.log(this.state.isOpen)
    this.setState({isOpen: false})
    this.forceUpdate();
    window.location.reload(false);

    // return <Redirect to="/youtube" />
  }
 
  render () {
    console.log("test")
    return (
      <div>
      {this.state.type=='video'
      ?<ModalVideo channel='custom' isOpen={this.state.isOpen} url={`${PortAddress.server}/play/`+this.state.id+'&'+this.state.video+'&'+this.state.type} onClose={this.closeModel} />
      :<ReactAudioPlayer src={`${PortAddress.server}/play/`+this.state.id+'&'+this.state.video+'&'+this.state.type}
        autoPlay
        controls
        />
        }
      </div>
    )
  }
}

export default VideoPlayer
