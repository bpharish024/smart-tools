import React, { Component } from 'react'
import ToolCards from './ToolCards'

export class Tools extends Component {

    render() {
        return (
            <React.Fragment>
                    <ToolCards />
            </React.Fragment>
        )
    }
}

export default Tools
