import swal from 'sweetalert';
import { PortAddress } from './PortAddress'
import axios from 'axios'

export function loadRecents(uid){
    const query = {
        userId: uid
    }
    axios.post(`${PortAddress.server}/recentyoutube`, { query })
        .then(response => {
            console.log("test",response)
            // this.setState(
            //     { recents: response.data }
            // )
            return response
        })
        .catch(err => console.log(err))
}

export function deleteRecents(zipName, type, uid, cb){
    console.log(zipName)
    const query = {
        zipName: zipName,
        userId:uid,
        type:type
    }
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then((willDelete) => {
        if (willDelete) {
                axios.delete(`${PortAddress.server}/deleteRecent`, { data:query })
                .then(response=>{
                    console.log(response)
                    cb()
                })
                .catch(e => {
                    cb()
                })
    }
})}
