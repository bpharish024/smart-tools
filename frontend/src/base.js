import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";


const app = firebase.initializeApp({
    apiKey: "AIzaSyACeIGw0nef3H0sKAmmSxnHNNdUMMonjGk",
    authDomain: "smart-tools-6465c.firebaseapp.com",
    databaseURL: "https://smart-tools-6465c.firebaseio.com",
    projectId: "smart-tools-6465c",
    storageBucket: "smart-tools-6465c.appspot.com",
    messagingSenderId: "619298254820",
    appId: "1:619298254820:web:af1216b8c1e7a63bfbe1f1",
    measurementId: "G-710Y3FPQ10"
  });

export default app;