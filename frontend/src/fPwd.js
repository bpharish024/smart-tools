import React, { useCallback, useContext, useState } from "react";
import { withRouter, Redirect } from "react-router";
import app from "./base.js";
import {ToastsContainer, ToastsStore} from 'react-toasts';
import swal from 'sweetalert';

const FPwd = () => {
    const [sent, setSent] = useState(false)
    const [email, setEmail] = useState(null);
        const handleReset = useCallback(
            async event => {
                event.preventDefault();
                try {
                    await app
                        .auth()
                        .sendPasswordResetEmail(email);
                        swal("Success!", "Please follow the link in "+email+" to reset your password!", "OK");
                        setSent(true)
                } catch (error) {
                    ToastsStore.error(error.message)
                }
            },
    );

    const onChange = (e) => {
        setEmail(e.target.value)
    }

    if (sent){
        return <Redirect to="/" />;
    }

    return (
        <React.Fragment>
                <ToastsContainer store={ToastsStore}/>

            <div className="row">
            <div className="col-md-5 offset-md-7">
            <div className="col">
                <h1 className="justify-content-center mt-0 text-white">Confirm Email</h1>
            </div>
                <div className="col">
                    <form onSubmit={handleReset}>
                        <div className="form-group row">
                            {/* <label className="col-sm-2 col-form-label" for="exampleInputEmail1">Email</label> */}
                            <div className="col-sm-12">
                                <input type="text" onChange={onChange} className="form-control" id="email" name="email" placeholder="Enter registered email" />
                            </div>
                        </div>
                        <div className="col-sm-12">
                            <button type="submit" className="btn btn-primary">Confirm</button>
                        </div>
                    </form>
                </div>
                </div>
            </div>
        </React.Fragment>
    );
};

export default withRouter(FPwd);

