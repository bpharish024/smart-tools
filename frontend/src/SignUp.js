import React, { useCallback, useState, useEffect} from "react";
import { withRouter } from "react-router";
import app from "./base";
import {ToastsContainer, ToastsStore} from 'react-toasts';
var db = app.firestore();
const SignUp = ({ history }) => {
    const handleSignUp = useCallback(async event => {
    event.preventDefault();
    const { email, password, name, phone } = event.target.elements;
    try {
      await app
        .auth()
        .createUserWithEmailAndPassword(email.value, password.value)
            .then(async cred=>{
                console.log("hello",cred.user.uid)
                db.collection('users').doc(cred.user.uid).set({
                name:name.value,
                email:email.value,
                phone:phone.value
            }).catch(err => console.log(err))
        })
      history.push("/");
    } catch (error) {
        console.log(error.message)
      ToastsStore.error(error.message)

    }
  }, [history]);

    const [show, setShow] = useState(false)
    const [type, setType] = useState("password")
    const showPassword=()=>{
        setShow(!show)
        if(show){
            setType("password")
        }
        else{
            setType("text")
        }
    }

  return (
    <React.Fragment>
                <ToastsContainer store={ToastsStore}/>
            <div className="row">
            <div className="col-md-5 offset-md-7">
            <div className="col">
                <h1 className="justify-content-center mt-0 text-white">Register</h1>
            </div>
                <div className="col">
                    <form onSubmit={handleSignUp}>
                    <div className="form-group row">
                            {/* <label className="col-sm-2 col-form-label" for="exampleInputEmail1">Name</label> */}
                            <div className="col-sm-12">
                                <input type="text" className="form-control" id="name" name="name" placeholder="Enter your name" required/>
                            </div>
                        </div>
                        <div className="form-group row">
                            {/* <label className="col-sm-2 col-form-label" for="exampleInputEmail1">Email</label> */}
                            <div className="col-sm-12">
                                <input type="email" className="form-control" id="email" name="email" placeholder="Enter email" required/>
                            </div>
                        </div>
                        <div className="form-group row">
                            {/* <label className="col-sm-2 col-form-label" for="exampleInputEmail1">Phone</label> */}
                            <div className="col-sm-12">
                                <input type="tel" className="form-control" id="phone" pattern= "[0-9]+" name="phone" placeholder="Enter phone number" required/>
                            </div>
                        </div>
                        <div className="form-group row">
                            {/* <label className="col-sm-2 col-form-label" for="password">Password</label> */}
                            <div className="input-group col-sm-12">
                                <input type={type} className="form-control"  id="password" name="password" placeholder="Password" required/>
                                <div class="input-group-append">
                                {show
                                ?<span style={{backgroundColor: "white",borderLeft:0,lineHeight:"inherit"}} class="btn input-group-text fas fa-eye-slash" onClick={showPassword} data-toggle="tooltip" data-placement="right" title="Hide password" name="searchKeyword" id="basic-addon2"></span>
                                :<span style={{backgroundColor: "white",borderLeft:0,lineHeight:"inherit"}} class="btn input-group-text fas fa-eye" onClick={showPassword} data-toggle="tooltip" data-placement="right" title="Show password" name="searchKeyword" id="basic-addon2"></span>
                                }
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-12">
                            <button type="submit" className="btn btn-primary">Register</button>
                        </div>
                    </form>
                </div>
                </div>
            </div>
        </React.Fragment>
  );
};

export default withRouter(SignUp);
