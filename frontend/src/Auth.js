import React, { useEffect, useState } from "react";
import app from "./base.js";
import ClipLoader from "react-spinners/ClipLoader";

export const AuthContext = React.createContext();

export const AuthProvider = ({ children }) => {
  const [currentUser, setCurrentUser] = useState(null);
  const [user, setUser] = useState(null)
  const [pending, setPending] = useState(true);

  useEffect(() => {
    app.auth().onAuthStateChanged((user) => {
      setCurrentUser(user)
      setPending(false)
    });
  }, []);

  if (pending) {
    return <div className="container">
      <div className="row">
        <div style={{
          position: "fixed",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          transform: "-webkit-translate(-50%, -50%)",
          transform: "-moz-translate(-50%, -50%)",
          transform: "-ms-translate(-50%, -50%)",
          color: "darkred"
        }} className="col-md-3 col-md-offset-4 justify-content-center" className="sweet-loading">
          <ClipLoader
            size={50}
            color={"#123abc"}
            loading="true"
          />
        </div>
      </div>
    </div>
  }

  return (
    <AuthContext.Provider
      value={{
        currentUser, setCurrentUser, user, setUser
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
