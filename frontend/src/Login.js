import React, { useCallback, useContext, useState } from "react";
import { withRouter, Redirect } from "react-router";
import app from "./base.js";
import { AuthContext } from "./Auth.js";
import { ToastsContainer, ToastsStore } from 'react-toasts';
import { Link } from 'react-router-dom'
import Backgroundimg from './images/smartToolBackground.webp'
const Login = ({ history }) => {
    const { currentUser } = useContext(AuthContext);
    const handleLogin = useCallback(
        async event => {
            event.preventDefault();
            const { email, password } = event.target.elements;
            try {
                await app
                    .auth()
                    .signInWithEmailAndPassword(email.value, password.value);
                console.log(email)
                history.push("/");
            } catch (error) {
                ToastsStore.error(error.message)

            }
        },
        [history]
    );

    const [email, setEmail] = useState(null);
    // const handleReset = useCallback(
    //     async event => {
    //         event.preventDefault();
    //         try {
    //             await app
    //                 .auth()
    //                 .sendPasswordResetEmail(email);
    //             ToastsStore.success("Reset Password link sent to email, please check")
    //         } catch (error) {
    //             ToastsStore.error(error.message)
    //         }
    //     },
    // );

    const [show, setShow] = useState(false)
    const [type, setType] = useState("password")
    const showPassword = () => {
        setShow(!show)
        if (show) {
            setType("password")
        }
        else {
            setType("text")
        }
    }

    const onChange = (e) => {
        setEmail(e.target.value)
    }


    if (currentUser) {
        return <Redirect to="/" />;
    }

    return (
        <React.Fragment>
            <ToastsContainer store={ToastsStore} />

            <div className="row">
                <div className="col-md-5 offset-md-7">
                <div className="col">
                    <h1 className="justify-content-center mt-0 text-white">Log in</h1>
                </div>
                <div className="col">
                    <form onSubmit={handleLogin}>
                        <div className="form-group row">
                            {/* <label className="col-sm-2 col-form-label" for="exampleInputEmail1">Email</label> */}
                            <div className="input-group col-sm-12">
                                <input type="email" onChange={onChange} className="form-control" id="email" name="email" placeholder="Enter registered email" />
                            </div>
                        </div>
                        <div className="form-group row">
                            {/* <label className="col-sm-2 col-form-label" for="password">Password</label> */}
                            <div className="input-group col-sm-12">
                                <input type={type} className="form-control" id="password" name="password" placeholder="Password" />
                                <div class="input-group-append">
                                    {show
                                        ? <span style={{ backgroundColor: "white", borderLeft: 0, lineHeight: "inherit" }} class="btn input-group-text fas fa-eye-slash" onClick={showPassword} data-toggle="tooltip" data-placement="right" title="Hide password" name="searchKeyword" id="basic-addon2"></span>
                                        : <span style={{ backgroundColor: "white", borderLeft: 0, lineHeight: "inherit" }} class="btn input-group-text fas fa-eye" onClick={showPassword} data-toggle="tooltip" data-placement="right" title="Show password" name="searchKeyword" id="basic-addon2"></span>
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-12">
                            <button type="submit" className="btn btn-primary">Login</button>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-12">
                                <Link to={"/fpwd"} className="btn text-primary">Forgot Password?</Link>
                            </div>
                        </div>
                    </form>
                </div>

                </div>
            </div>
        </React.Fragment>
    );
};

export default withRouter(Login);
