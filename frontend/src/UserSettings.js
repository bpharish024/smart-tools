import React, { useCallback, useState, useContext, useEffect, Component} from "react";
import { withRouter } from "react-router";
import app from "./base";
import { AuthContext } from "./Auth";
import { ToastsContainer, ToastsStore } from 'react-toasts';
import swal from 'sweetalert';

var db = app.firestore();
export class UserSettings extends Component {
    static contextType = AuthContext;
    state={
        name:"",
        email:"",
        phone:"",
        currentuser:this.context.currentUser.uid,
        enable:false,
    }
    componentDidMount=()=> {
        this.loadData()
}
loadData=()=>{
    var docRef = db.collection("users").doc(this.state.currentuser);
        console.log(this.state.currentuser)
        if (docRef) {
        docRef.get().then((doc)=>{
            if (doc.exists) {
                this.setState({name:doc.data()['name']})
                this.setState({email:(doc.data()['email'])})
                this.setState({phone:doc.data()['phone']})
                console.log("test")
            } else {
                this.setState({name:"user"})
            }
        }).catch((error)=>{
            ToastsStore.error(error.message)
            console.log("Error getting document:", error);
        });
    }
}
    onchange=(event)=>{
        // event.preventDefault();
        this.setState({[event.target.name]:event.target.value})
    }
    updateUser=(event)=> {
        event.preventDefault()
        var docRef = db.collection("users").doc(this.state.currentuser);
        var user = app.auth().currentUser;

        user.updateEmail(this.state.email).then(()=>{
            // Update successful.
            docRef.update({
                "name": this.state.name,
                "email": this.state.email,
                "phone": this.state.phone
            })
            .then(()=> {
                console.log("Document successfully updated!");
                this.setState({enable:!this.state.enable})
                window.location.reload()
            });
          }).catch((error)=>{
            console.log('Error updating user in auth:', error);
            ToastsStore.error(error.message)
            this.setState({enable:!this.state.enable})
          });
    }
    enableToggle=()=>{
        this.setState({enable:!this.state.enable})
    }
    rereshValue=()=>{
        this.loadData()
    }
    deleteUser=()=> {
        var user = app.auth().currentUser;
        var docRef = db.collection("users").doc(this.state.currentuser);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this account!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                user.delete().then(()=> {
                    console.log('Successfully deleted user in auth');
                    docRef.delete().then(()=> {
                        console.log('Successfully deleted user in database');
                    }).catch((error)=> {
                        console.log('Error deleting user in database:', error);
                    });
                    swal("Poof! Your account has been deleted!", {
                        icon: "success",
                      });
                }).catch((error)=> {
                    console.log('Error deleting user in auth:', error);
                });
            } else {
              swal("Your account is safe!");
            }
          });
    }
    render(){
    return (
        <React.Fragment>
            <ToastsContainer store={ToastsStore} />
            <div className="row">
                <div className="col-md-12 mb-3">
                    <button type="button" onClick={this.deleteUser} className="btn btn-sm btn-danger mr-2">Delete Account</button>
                    <button type="button" onClick={this.enableToggle} className="btn btn-sm btn-dark mr-2">Edit Details</button>
                    {/* <button type="button" onClick={this.rereshValue} className="btn btn-sm btn-dark">Refresh</button> */}
                </div>
            </div>
            <div className="row">
                <div className="col-sm-8 offset-sm-3">
                    <form onSubmit={this.updateUser}>
                        <div className="form-group row">
                            {/* <label className="col-sm-2 col-form-label" for="exampleInputEmail1">Name</label> */}
                            <div className="col-sm-6 offset-sm-2">
                                {this.state.enable
                                ?<input type="text" onChange={this.onchange} value={this.state.name} className="form-control" id="name" name="name" placeholder="Enter your name" required />
                                :<input type="text" disabled value={this.state.name} className="form-control" id="name" name="name" placeholder="Enter your name" required />
                                }
                            </div>
                        </div>
                        <div className="form-group row">
                            {/* <label className="col-sm-2 col-form-label" for="exampleInputEmail1">Email</label> */}
                            <div className="col-sm-6 offset-sm-2">
                                {this.state.enable
                                ?<input type="email" onChange={this.onchange} value={this.state.email} className="form-control" id="email" name="email" placeholder="Enter email" required />
                                :<input type="email" disabled value={this.state.email} className="form-control" id="email" name="email" placeholder="Enter email" required />
                                }
                            </div>
                        </div>
                        <div className="form-group row">
                            {/* <label className="col-sm-2 col-form-label" for="exampleInputEmail1">Phone</label> */}
                            <div className="col-sm-6 offset-sm-2">
                                {this.state.enable
                                ?<input type="tel" onChange={this.onchange} value={this.state.phone} pattern= "[0-9]+" className="form-control" id="phone" name="phone" placeholder="Enter phone number" required />
                                :<input type="tel" disabled value={this.state.phone} className="form-control" id="phone" name="phone" placeholder="Enter phone number" required />
                                }
                            </div>
                        </div>
                        <div className="col-sm-9">
                            {this.state.enable
                            ?<button type="submit" className="btn btn-primary">Save</button>
                            :<button disabled type="submit" className="btn btn-primary">Save</button>
                            }
                            <button type="button" onClick={this.rereshValue} className="btn btn-danger ml-2">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </React.Fragment>
    );
}
}

export default UserSettings;
